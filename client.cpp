/* 
 * Soubor:    client.cpp
 * Vytvořeno: 14. březen 2015, 15:54
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt č. 1 do předmětu IPK, klient žádá server o informace ze souboru passwd 
 */

#include <cstdlib>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <string>
#include <regex>
#include <vector>


using namespace std;

/**
 * Funkce zjistí, jestli je řetězec složen pouze z číslic
 * @param str řetezec, který ověřujeme
 * @return true nebo false, v závislosti na tom, jestli řetězec obsahuje číslice
 */
bool str_has_only_digits(string str);

/**
 * Funkce zpracuje parametry a jejich hodnoty předá pomocí odkazů do odpovídajících proměnných.
 * V případě chyby funkce ukončuje program
 * @param argc počet argumentů předaných programu
 * @param argv argumenty předané programu
 * @param hostname doménové jméno serveru, na který se bude připojovat klient
 * @param port port serveru, na kterém bude očekávat klientské žádosti
 * @param vectorOfLogins pole loginů, pro které chce uživatel získat záznamy
 * @param vectorOfUIDs pole uid, pro které chce uživatel získat záznamy
 * @param itemsToPrint položky, které si přeje uživatel vytisknout pro každý záznam v passwd
 * @param loginFlag příznak, že záznamy v passwd budeme vyhledávat podle loginu
 * @param uidFlag příznak, že záznamy v passwd budeme vyhledávat podle uid
 * @return void
 */
void processTheParams(int argc, char** argv, string* hostname, string* port, 
                      vector<string>* vectorOfLogins, vector<string>* vectorOfUIDs,
                      vector<string>* itemsToPrint, bool* loginFlag, bool* uidFlag);

/**
 * Funkce pro zaslání zprávy serveru, která bude specifikovat, která data si ze souboru passwd přejeme
 * Pokud dojde k chybě funkce ukončuje program
 * @param sockfd deskriptor socketu, prostřednictvím kterého budeme data zasílat na server
 * @param uidFlag příznak, že záznamy v passwd budeme vyhledávat podle uid, pokud je false, vyhledáváme podle loginu
 * @param vectorOfLogins pole loginů, pro které chce uživatel získat záznamy
 * @param vectorOfUIDs pole uid, pro které chce uživatel získat záznamy
 * @param itemsToPrint položky, které si přeje uživatel vytisknout pro každý záznam v passwd
 * @return void
 */
void askTheServerForAData(int* sockfd, bool uidFlag, vector<string>* vectorOfLogins, 
                          vector<string>* vectorOfUIDs, vector<string>* itemsToPrint);

/**
 * Funkce pro získání zprávy ze serveru, která obsahuje požadované data. Funkce tuto zprávu také vytiskne.
 * Pokud dojde k chybě funkce ukončuje program
 * @param sockfd deskriptor socketu, prostřednictvím kterého budeme číst data ze serveru
 * @return void
 */
void getAndPrintDataFromServer(int* sockfd);

/**
 * Klasický main
 */
int main(int argc, char** argv) {
    
    string hostname;               // název serveru, který se později přeloží na ip adresu
    string port;                   // port na kterém očekává aplikace na serveru klientskou žádost
    vector<string> vectorOfLogins; // pole jednotlivých zadaných loginů
    vector<string> vectorOfUIDs;   // pole jednotlivých zadaných uid
    vector<string> itemsToPrint;   // řetezec položek, které uživatel požaduje, aby mu server vrátil v rámci údajů o uživateli
    
    bool loginFlag = false;        // příznak, který určuje, jestli budeme vyhledávat záznamy v passwd podle loginu
    bool uidFlag = false;          // příznak, který určuje, jestli budeme vyhledávat záznamy v passwd podle uid

    // zpracujeme a načteme parametry programu
    processTheParams(argc, argv, &hostname, &port, &vectorOfLogins, &vectorOfUIDs, 
                     &itemsToPrint, &loginFlag, &uidFlag);
    
    int sockfd;             // deskriptor klientského socketu
    struct hostent *hptr;   // struktua obsahující mj. IP adresu serveru
    struct sockaddr_in sin; // struktua obsahující informace, důležité pro komunikaci mezi klientem a serverem
    
    // vytvoříme socket
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0 )) < 0) {
        // chyba při vytváření socketu 
        perror("Chyba, nepodařilo se vytvořit socket"); 
        exit(EXIT_FAILURE);
    }
    
    sin.sin_family = AF_INET;                   // nastavíme protokol
    sin.sin_port = htons(atoi(port.c_str()));   // nastavíme číslo portu serveru
  
    // zkusíme přeložit jméno serveru na ip adresu
    if ( (hptr =  gethostbyname(hostname.c_str())) == NULL) {
        // překlad doménového jména selhal, ukončíme program
        fprintf(stderr, "Chyba: nepodařilo se přeložit název: %s na IP adresu\n", hostname.c_str());
        close(sockfd);
        exit(EXIT_FAILURE);
    }
  
    // nastavíme ip adresu serveru
    memcpy( &sin.sin_addr, hptr->h_addr, hptr->h_length);
    
    // pokusíme se připojit k serveru
    if (connect (sockfd, (struct sockaddr *)&sin, sizeof(sin) ) < 0 ) {
        // pokud se připojení nezdařilo, ukončíme program
        perror("Chyba, nepodařilo se připojit k serveru"); 
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    
    // zašleme serveru zprávu, ve které specifikujeme o která data jej budeme žádat
    askTheServerForAData(&sockfd, uidFlag, &vectorOfLogins, &vectorOfUIDs, &itemsToPrint);
    
    // získáme zprávu ze serveru, která obsahuje požadovaná data a zároveň tyto data vytiskneme
    getAndPrintDataFromServer(&sockfd);
    
    // uzavřeme socket
    if (close(sockfd) < 0) { 
        // pokud uzavření selhalo, ukončíme program
        perror("Chyba, nepodařilo se uzavřít socket"); 
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS); // úspěšně ukončíme program
}

bool str_has_only_digits(string str) {
    return str.find_first_not_of("0123456789") == string::npos;
}

void processTheParams(int argc, char** argv, string* hostname, string* port, 
                      vector<string>* vectorOfLogins, vector<string>* vectorOfUIDs,
                      vector<string>* itemsToPrint, bool* loginFlag, bool* uidFlag) {
    
    int ch;                     // proměnná, do které getopt ukládá název aktuálně načteného parametru
    string uid;                 // dočasná proměnná, do které načteme vždy aktuální uid
    bool hostnameFlag = false;  // příznak, určující jestli byl zadán název serveru, na kterém běží server
    bool portFlag = false;      // příznak, určující jestli byl zadán port serveru, na kterém se budou očekávat klientské žádosti
    
    bool printUserNameFlag = false;  // příznak, jestli byl zadán parametr -L
    bool printUIDFlag = false;       // příznak, jestli byl zadán parametr -U
    bool printGIDFlag = false;       // příznak, jestli byl zadán parametr -G
    bool printGecosFlag = false;     // příznak, jestli byl zadán parametr -N
    bool printHomeDirFlag = false;   // příznak, jestli byl zadán parametr -H
    bool printLogShellFlag = false;  // příznak, jestli byl zadán parametr -S

    
    opterr = 0; // vypneme výchozí výpis chyb spojených s argumenty
    // pomocí getopt zpracujeme argumenty
    while ((ch = getopt(argc, argv, "u:l:h:p:LUGNHS")) != EOF) {
        
        switch (ch) {
            
            // parametr l pro login(y)
            case 'l':
                *uidFlag = false;  // vyhledáváme jen podle jednoho kritéria
                *loginFlag = true; // a tím kritérie bude login
                
                // uložíme login do pole loginů
                (*vectorOfLogins).push_back(string(optarg));
                /* uživatel mohl zadat více loginů, takže projdeme následující argumenty
                   a dokud nenarazíme na přepínač, nebo konec vstupu, tak považujeme argumenty za loginy a 
                   postupně přidáváme loginy do pole loginů */
                while(true) {
                    if (optind >= argc || argv[optind][0] == '-') {       
                        break;
                    }
                    (*vectorOfLogins).push_back(string(argv[optind])); // uložíme login do pole loginů
                    optind++;
                }               
                break;
            
            // parametr u pro uid(s)
            case 'u':
                *loginFlag = false; // vyhledáváme jen podle jednoho kritéria
                *uidFlag = true;    // a tím kritérie bude login
                uid = string(optarg);
                
                // zkontrolujem zadané uid a pokud není číslo vrátíme chybu
                if (!str_has_only_digits(uid)) {
                    fprintf(stderr, "Chyba: Zadané UID není číslo!\n");
                    exit(EXIT_FAILURE);
                }
                
                // pokud je zadané uid v pořádku, přidáme jej do pole uid
                (*vectorOfUIDs).push_back(uid);
                
                /* uživatel mohl zadat více uid, takže projdeme následující argumenty
                   a dokud nenarazíme na přepínač, nebo konec vstupu, tak považujeme argumenty za uid a 
                   postupně přidáváme uid do pole ui */
                while(true) {
                    if (optind >= argc || argv[optind][0] == '-') {       
                        break;
                    }
                    uid = string(argv[optind]);
                    // pokud zadané uid není číslo vrátíme chybu
                    if (!str_has_only_digits(uid)) {
                        fprintf(stderr, "Chyba: Zadané UID není číslo!\n");
                        exit(EXIT_FAILURE);
                    }
                    (*vectorOfUIDs).push_back(uid); // pokud je zadané uid v pořádku, přidáme jej do seznamu uid
                    optind++;
                }               
                break;
            
            // parametr h pro doménové jméno serveru
            case 'h':
                hostnameFlag = true;
                *hostname = string(optarg);
                break;
            
            // parametr p, pro port serveru, na kterém očekává klientské žádosti
            case 'p':
                portFlag = true;
                *port = string(optarg);
                
                // pokud zadané uid není číslo vrátíme chybu
                if (!str_has_only_digits(*port)) {
                    fprintf(stderr, "Chyba: Zadaný port není číslo!\n");
                    exit(EXIT_FAILURE);
                }
                
                // ověříme jestli se port nachází v rozsahu povoleném a případně vrátíme chybu
                int portNum;
                portNum = atoi((*port).c_str()); 
                if (portNum < 1024 || portNum > 65535) {
                    fprintf(stderr, "Chyba: Zadaný port se nachází v nepovoleném rozsahu!\n");
                    exit(EXIT_FAILURE);
                }                      
                break;
            
            // požadujeme v rámci záznamu passwd položku L (login)
            case 'L':
                // pokud již byl tento parametr zadán, nebudeme jej znovu zpracovávat a přeskočíme jej
                if (printUserNameFlag) break;
                
                // přidáme položku do výsledného setu informací, který budeme chtít o uživateli zjistit
                (*itemsToPrint).push_back("L");
                printUserNameFlag = true; // nastavíme příznak, že jsme již tento parametr zpracovali
                break;
            
            // požadujeme v rámci záznamu passwd položku U (uid)
            case 'U':
                // pokud již byl tento parametr zadán, nebudeme jej znovu zpracovávat a přeskočíme jej
                if (printUIDFlag) break;
                
                // přidáme položku do výsledného setu informací, který budeme chtít o uživateli zjistit
                (*itemsToPrint).push_back("U");
                printUIDFlag = true; // nastavíme příznak, že jsme již tento parametr zpracovali
                break;
                
            // požadujeme v rámci záznamu passwd položku G (gid)
            case 'G':
                // pokud již byl tento parametr zadán, nebudeme jej znovu zpracovávat a přeskočíme jej
                if (printGIDFlag) break;
                
                // přidáme položku do výsledného setu informací, který budeme chtít o uživateli zjistit
                (*itemsToPrint).push_back("G");
                printGIDFlag = true; // nastavíme příznak, že jsme již tento parametr zpracovali
                break;
                
            // požadujeme v rámci záznamu passwd položku N (gecos)
            case 'N':
                // pokud již byl tento parametr zadán, nebudeme jej znovu zpracovávat a přeskočíme jej
                if (printGecosFlag) break;

                // přidáme položku do výsledného setu informací, který budeme chtít o uživateli zjistit
                (*itemsToPrint).push_back("N");
                printGecosFlag = true; // nastavíme příznak, že jsme již tento parametr zpracovali
                break;
                
            // požadujeme v rámci záznamu passwd položku H (domovský adresář)
            case 'H':
                // pokud již byl tento parametr zadán, nebudeme jej znovu zpracovávat a přeskočíme jej
                if (printHomeDirFlag) break;
                
                // přidáme položku do výsledného setu informací, který budeme chtít o uživateli zjistit
                (*itemsToPrint).push_back("H");
                printHomeDirFlag = true; // nastavíme příznak, že jsme již tento parametr zpracovali
                break;
                
            // požadujeme v rámci záznamu passwd položku S (shell výchozí)
            case 'S':
                // pokud již byl tento parametr zadán, nebudeme jej znovu zpracovávat a přeskočíme jej
                if (printLogShellFlag) break;

                // přidáme položku do výsledného setu informací, který budeme chtít o uživateli zjistit
                (*itemsToPrint).push_back("S");
                printLogShellFlag = true; // nastavíme příznak, že jsme již tento parametr zpracovali
                break;
                
            // pokud dojde k chybě (typicky neznámý parametr, chybějící hodnota parametru)
            default:
                fprintf(stderr, "Chyba: Nesprávný formát argumentů/parametrů!\n");
                exit(EXIT_FAILURE);
                break;
        }
    }

    // pokud byl(y) zadán argument(y) navíc, dojde k chybě
    if (argc != optind) {
       fprintf(stderr, "Chyba: Nesprávný formát argumentů/parametrů!\n");
       exit(EXIT_FAILURE);
    }
    
    // ověříme, že byly zadány všechny povinné parametry
    if (!(portFlag && (*uidFlag || *loginFlag) && hostnameFlag)) {
       fprintf(stderr, "Chyba: Nebyly zadány všechny povinné parametry!\n");
       exit(EXIT_FAILURE);
    }
        
}

void askTheServerForAData(int* sockfd, bool uidFlag, vector<string>* vectorOfLogins, 
                          vector<string>* vectorOfUIDs, vector<string>* itemsToPrint) {

    // Jediná zpráva, která se bude zasílat serveru, 
    // bude obsahovat zabalené v protokolu vše co zadal klient jako argumenty
    string messageToSend;
    messageToSend += "<?xml version=\"1.0\"?>";
    messageToSend += "<message>";
    
    // v odesílané zprávě specifikujeme o které položky ze souboru passwd máme zájem
    messageToSend += "<required_items>";
    for (unsigned int i = 0; i < (*itemsToPrint).size(); i++) {
        messageToSend += "<required_item>";
        messageToSend += (*itemsToPrint)[i];
        messageToSend += "</required_item>";
    }
    messageToSend += "</required_items>";
    
    // pokud budeme uživatele hledat podle uid, specifikujeme to v protokolu
    if (uidFlag){
        messageToSend += "<search_by>UID</search_by>";
        messageToSend += "<searched_users>";
        for (unsigned int i = 0; i < (*vectorOfUIDs).size(); i++) {
            messageToSend += "<searched_user>";
            messageToSend += (*vectorOfUIDs)[i];
            messageToSend += "</searched_user>";
        }
        messageToSend += "</searched_users>";
    }
    // pokud budeme uživatele hledat podle uživatelksého jména, specifikujeme to v protokolu
    else{
        messageToSend += "<search_by>LOGIN</search_by>";
        messageToSend += "<searched_users>";
        for (unsigned int i = 0; i < (*vectorOfLogins).size(); i++) {
            messageToSend += "<searched_user>";
            messageToSend += (*vectorOfLogins)[i];
            messageToSend += "</searched_user>";
        }
        messageToSend += "</searched_users>";
    }
    
    messageToSend += "</message>";
    
    int bytes_sent; // proměnná, která určuje kolik bajtů poslala funkce write
    
    // postupně odešleme všechna data na server
    while(1) {
        // pokusíme se odeslat všechnadata na server
        if ((bytes_sent = write(*sockfd, messageToSend.c_str(), messageToSend.length())) < 0 ) {
            // chyba při odesílání dat, ukončíme program
            perror("Chyba, nepodařilo se odeslat data na server"); 
            close(*sockfd);
            exit(EXIT_FAILURE);
        }
        // pokud z nějakého důvodu nemohla být všechna data zaslána na server, 
        // připravíme poslání zbytku dat pro další iteraci
        if ((unsigned int) bytes_sent != messageToSend.length()) {
            messageToSend = messageToSend.substr(bytes_sent, string::npos);
        }
        // pokud byla všechna data úspěšeně odeslána vyskočíme ze smyčky
        else {
            break; 
        }
    }    
}

void getAndPrintDataFromServer(int* sockfd) {

    char msgBufReader[256];  // buffer, do kterého budeme postupně načítat odpověd serveru (s daty)
    int bytes_received;      // kolik bajtů jsme pomocí funkce read přečetli
    string messageReceived;  // zpráva, kterou jsme od serveru získali
    
    // postupně získáme všechna data od serveru
    while (1) {
        bzero(msgBufReader, 256); // inicializujeme buffer
        // Pokusíme se získat všechna data od klienta
        if ((bytes_received = read(*sockfd, msgBufReader, 256)) < 0) {
            // chyba při čtení dat, ukončíme program
            perror("Chyba, data nemohla být přijata ze serveru");
            close(*sockfd);
            exit(EXIT_FAILURE);
        }
        // pokud server ukončil spojení dřív než jsme přečetli celou zprávu, je to chyba
        else if(bytes_received == 0) {
            fprintf(stderr, "Chyba: server nečekaně ukončil spojení!\n");
            close(*sockfd);
            exit(EXIT_FAILURE);
        }
        // přidáme data načtená v této iteraci do výsledného přečtěného řetězce
        messageReceived += string(msgBufReader).substr(0, bytes_received);

        // pokud jsme přijali celou zprávu, vyskočíme ze smyčky
        if (messageReceived.find("</message>") != string::npos) {
            break;
        }
    }
    
    // regulární výraz, pro získání jednotlivých záznamů
    regex patternRecords("<record type=\"(normal|error)\">(.*?)</record>");
    { // pomocí regex iterátoru získám jednotlivé položky
        sregex_iterator end;
        for (sregex_iterator next(messageReceived.begin(), messageReceived.end(), patternRecords);
                next != end;
                ++next) {
            smatch match = *next;
            // pokud je záznam v pořádku, vypíšeme jej na stdout
            if (string(match[1].str()) == "normal") {
                cout << string(match[2].str()) << endl;
            }
            // jinak jej vypíšeme na stderr
            else {
                fprintf(stderr, "%s\n", string(match[2].str()).c_str());
            }
        }
    }
}