/* 
 * Soubor:    server.cpp
 * Vytvořeno: 14. březen 2015, 16:00
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt č. 1 do předmětu IPK, server vrací klientovi požadované informace ze souboru passwd 
 */

#include <cstdlib>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <string>
#include <regex>
#include <vector>
#include <fstream>
#include <strings.h>
#include <sys/wait.h>
#include <signal.h>

using namespace std;

// konstanty, podle jedné z nich hledáme v passwd
enum searchByEnumeration {
    LOGIN,
    UID
};

/**
 * Funkce zjistí, jestli je řetězec složen pouze z číslic
 * @param str řetezec, který ověřujeme
 * @return true nebo false, v závisloti na tom, jestli řetězec obsahuje číslice
 */
bool str_has_only_digits(string str);

/**
 * Funkce která obsluhuje ukončení potomka hlavního procesu
 * @param sig typ signálu
 * @return void
 */
void sigchld_handler(int sig);

/**
 * Funkce zpracuje parametry a jejich hodnoty předá pomocí odkazů do odpovídajících proměnných.
 * V případě chyby funkce ukončuje program
 * @param argc počet argumentů předaných programu
 * @param argv argumenty předané programu
 * @param port port serveru, na kterém bude očekávat klientské žádosti
 * @return void
 */
void processTheParams(int argc, char** argv, string* port);

/**
 * Funkce přijímá nové klienty, kteří se připojí na server
 * Pokud dojde k chybě funkce ukončuje program, nebo jen podproces pro určitého klienta
 * @param sockfd deskriptor socketu, prostřednictvím kterého budeme přijímat nové žádost od klientů
 * @return void
 */
void acceptClients(int* sockfd);

/**
 * Funkce zpracuje klientskou žádost o data
 * Pokud dojde k chybě funkce ukončuje program, nebo jen podproces pro určitého klienta
 * @param new_fd deskriptor socketu, prostřednictvím kterého je podproces serveru navázán na určitého klienta
 * @param messageToSend zde uložíme zprávu, která se zašle klientovi
 * @return void
 */
void processTheClientsRequest(int* new_fd, string* messageToSend);

/**
 * Funkce vrátí klientovi požadovaná data
 * Pokud dojde k chybě funkce ukončuje program, nebo jen podproces pro určitého klienta
 * @param new_fd deskriptor socketu, prostřednictvím kterého je podproces serveru navázán na určitého klienta
 * @param messageToSend zpráva, kterou funkce zašle klientovi
 * @return void
 */
void sendRequiredDataToClient(int* new_fd, string* messageToSend);

/*
 * Klasický main
 */
int main(int argc, char** argv) {

    // registrujeme obslužnou rutinu při ukončení potomků hl. procesu serveru
    signal(SIGCHLD,sigchld_handler);

    string port; // port na kterém očekává server klientskou žádost
    
    // zpracujeme a načteme parametry programu
    processTheParams(argc, argv, &port);
    
    int sockfd;              // deskriptor socketu serveru, na kterém bude naslouchat požadavkům
    struct sockaddr_in sin;  // informace o mojí adrese
    
    // vytvoříme socket
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0 )) < 0) {
        // chyba při vytváření socketu 
        perror("Chyba, nepodařilo se vytvořit socket"); 
        return -1;
    }
    
    // zajistíme to, že můžeme bindovat bez chyb
    int optval = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int)) < 0) {
        /* Chyba při nastavování voleb u socketu */
        perror("Chyba, selhalo volání setsockopt"); 
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    
    sin.sin_family = AF_INET;                   // nastavíme protokol
    sin.sin_port = htons(atoi(port.c_str()));   // nastavíme číslo portu serveru
    sin.sin_addr.s_addr = htonl(INADDR_ANY);    // naszavíme IP adresu
    memset(&(sin.sin_zero), '\0', 8);           // zbytek struktury vyplníme 0
    
    // Bindneme socket na port
    if (bind(sockfd, (struct sockaddr *)&sin, sizeof(struct sockaddr) ) < 0 ) {
        // chyba při bindování, ukončíme program
        perror("Chyba, nepodařilo se bindnout socket na zadaný port"); 
        close(sockfd);
        exit(EXIT_FAILURE);
    }
   
    // Nasloucháme na portu, abychom mohli zpracovat příchozí požadavky klientů
    if (listen(sockfd, 5) < 0) { 
        // chyba při pokudu naslochat, ukončíme program
        perror("Chyba, nezdařil se pokud naslouchat na zadaném portu");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    
    // Budeme přijímat jednotlivé klienty a vyřizovat jejich žádosti
    acceptClients(&sockfd);

    exit(EXIT_SUCCESS); // zde se nejspíše nikdy nedostaneme
}

bool str_has_only_digits(string str) {
    return str.find_first_not_of("0123456789") == string::npos;
}

void sigchld_handler(int sig) {
    while (waitpid(-1, NULL, WNOHANG) > 0);
    return;
}

void processTheParams(int argc, char** argv, string* port) {

    int ch;                 // proměnná, do které getopt ukládá název aktuálně načteného parametru
    bool portFlag = false;  // příznak, určující jestli byl zadán port serveru, na kterém se budou očekávat klientské žádosti

    
    opterr = 0; // vypneme výchozí výpis chyb spojených s argumenty
    // pomocí getopt zpracujeme argumenty
    while ((ch = getopt(argc, argv, "p:")) != EOF) {
        switch (ch) {
            // parametr p, pro port serveru, na kterém očekává klientské žádosti
            case 'p':
                portFlag = true;
                *port = string(optarg);
                
                // pokud zadané uid není číslo vrátíme chybu
                if (!str_has_only_digits(*port)) {
                    fprintf(stderr, "Chyba: Zadaný port není číslo!\n");
                    exit(EXIT_FAILURE);
                }
                
                // ověříme jestli se port nachází v rozsahu povoleném
                int portNum;
                portNum = atoi((*port).c_str()); 
                if (portNum < 1024 || portNum > 65535) {
                    fprintf(stderr, "Chyba: Zadaný port se nachází v nepovoleném rozsahu!\n");
                    exit(EXIT_FAILURE);
                }   
                break;
            default:
                fprintf(stderr, "Chyba: Nesprávný formát argumentů/parametrů!\n");
                exit(EXIT_FAILURE);
                break;
        }
    }
    
    // ověříme, že byly zadány všechny povinné parametry
    if (portFlag == false) {
       fprintf(stderr, "Chyba: Chybí povinný parametr -p!\n");
       exit(EXIT_FAILURE); 
    }

    // pokud byl(y) zadán argument(y) navíc, dojde k chybě
    if (argc != optind) {
       fprintf(stderr, "Chyba: Nesprávný formát argumentů/parametrů!\n");
       exit(EXIT_FAILURE);
    }
}

void acceptClients(int* sockfd) {

    int new_fd;                    // deskriptor socketu serveru, na kterém bude navázán konkrétní klient
    struct sockaddr_in their_addr; // informace o adrese klienta
    
    int sin_size = sizeof(struct sockaddr);
    
    // klíčová smyčka, ve které přijímáme nové klienty a jejich žádosti
    while (1) {
        // přijímáme kladně žádost o nové spojení od klienta
        if ( (new_fd = accept(*sockfd, (struct sockaddr *) &their_addr, (socklen_t*)&sin_size) ) < 0 ) {
            // při chybě ukončujeme program
            perror("Chyba, nepodařilo se přijmout žádost o nové spojení iniciované klientem");
            close(*sockfd);
            exit(EXIT_FAILURE);
        }
        
        // vytvoříme nový podproces pro klienta
        int pid = fork();
        
        // fork se nezdařil
        if (pid < 0) {
            perror("Chyba, Fork se nezdařil");
            close(new_fd);
            close(*sockfd);
            exit(EXIT_FAILURE); 
        }
        // nový proces pro obsloužení nového klienta
        if (pid == 0) {
            
            // zavřeme deskriptor socketu, který jsme zdědili po rodiči, snížíme tak count u tohoto socketu
            if (close(*sockfd) < 0) { 
                perror("Chyba, při uzavírání socketu");  
                close(new_fd);
                exit(EXIT_FAILURE); 
            }
            
            string messageToSend;   // zpráva, která se pošle klientovi
            messageToSend.clear();  // gratuluji: nalezli jste easter egg - zbytečné použití operace clear() :-)

            // zpracujeme klientskou žádost o data
            processTheClientsRequest(&new_fd, &messageToSend);

            // vrátíme klientovy požadované data
            sendRequiredDataToClient(&new_fd, &messageToSend);
            
            // nakonec zavřeme socket
            if (close(new_fd) < 0) { 
                perror("Chyba, při uzavírání socketu"); 
                exit(EXIT_FAILURE);
            } 
            exit(EXIT_SUCCESS); // korektně ukončíme
        }
        // hlavní proces
        else {
            // zavřeme deskriptor socketu vzniklého po accept
            if (close(new_fd) < 0) { 
                perror("Chyba při uzavírání socketu");
                close(*sockfd);
                exit(EXIT_FAILURE); 
            }
        }
    }
}

void processTheClientsRequest(int* new_fd, string* messageToSend){
    
    char msgBufReader[256];    // buffer, kam budema načítat postupně vstup od uživatele
    int bytes_received;        // kolik bajtů jsme přijali od klienta v jednom volání read
    string messageReceived;    // celá zpráva, kterou jsme od klienta přijali
    messageReceived.clear();   // inicializujeme řetezec, nejspíše není nutné  
    
    // postupně získáme všechna data od klienta
    while(1) {
        bzero(msgBufReader,256); // inicializujeme buffer
        // Pokusíme se získat všechna data od klienta
        if ((bytes_received = read(*new_fd, msgBufReader, 256)) < 0) {
            // Chyba při čtení
            perror("Chyba, data nemohla být od klienta přijata"); 
            close(*new_fd);
            exit(EXIT_FAILURE);
        }
        // pokud klient ukončil spojení dřív než jsme přečetli celou zprávu, je to chyba
        else if(bytes_received == 0) {
            fprintf(stderr, "Chyba: Klient nečekaně ukončil spojení!\n");
            close(*new_fd);
            exit(EXIT_FAILURE);
        }

        // přidáme data načtená v této iteraci do výsledného přečtěného řetězce
        messageReceived += string(msgBufReader).substr(0, bytes_received); 

        // pokud jsme přijali celou zprávu, vyskočíme ze smyčky
        if (messageReceived.find("</message>") != string::npos) {
            break; 
        }
    }
    
    // regulární výraz, pro získání požadovaných položek záznamu o uživateli; ve spojení s regex_search
    regex patternRequireItems("<required_item>(L|U|G|N|H|S)</required_item>");
    // regulární výraz, pro určení podle jakého kritéria budem uživatele vyhledávat; ve spojení s regex_match
    regex patternSearchBy(".*<search_by>(LOGIN|UID)</search_by>.*");
    // regulární výraz, pro získání jednotlivých uživatelů; ve spojení s regex_search
    regex patternSearchedUsers("<searched_user>(.*?)</searched_user>");
    
    vector<string> vectorOfRequiredItems;  // pole požadovaných položek
    int searchBy;                          // udává jestli máme hledat záznamy podle loginu, nebo uid
    vector<string> vectorOfSearchedUsers;  // pole uživatelů, o nichž chce zobrazit záznamy
            
    // před použitím vyčistíme struktury, které mohou být zanesené smetím
    vectorOfRequiredItems.clear(); 
    vectorOfSearchedUsers.clear(); 

    { // pomocí regex iterátoru získám požadované položky záznamu o uživateli
        sregex_iterator end;
        for (sregex_iterator next(messageReceived.begin(), messageReceived.end(), patternRequireItems); next != end; ++next) {
            smatch match = *next;
            vectorOfRequiredItems.push_back(string(match[1].str()));
        }
    }

    { // pomocí regex iterátoru získám jednotlivé uživatele
        sregex_iterator end;
        for (sregex_iterator next(messageReceived.begin(), messageReceived.end(), patternSearchedUsers); next != end; ++next) {
            smatch match = *next;
            vectorOfSearchedUsers.push_back(string(match[1].str()));
        }
    }

    { // určím typ informace, podle které budu vyhledávat uživatele
        smatch match;
        regex_search(messageReceived, match, patternSearchBy);
        searchBy = (string(match[1].str()) == "LOGIN") ? LOGIN : UID;
    }
    
    // sestavíme obecný RV, který by měl najít všechny položky záznamu
    regex patternPasswd("((?:[\\.]|[a-zA-Z0-9]|[_])+(?:[\\.]|[a-zA-Z0-9]|[-]|[_])*):([^:]*):([[:digit:]]+):([[:digit:]]+):([^:]*):([^:]+):([^:]*)");


    string actualLine; // do této proměnné budeme ukládat aktuální řádek

    *messageToSend += "<?xml version=\"1.0\"?>";
    *messageToSend += "<message>";
    *messageToSend += "<records>";

    // pro každého uživatele najdeme jeho odpovídající záznam
    for (unsigned int i = 0; i < vectorOfSearchedUsers.size(); i++) {
        ifstream file("/etc/passwd"); // otevřeme soubor passwd
        // pokud se nepodařilo otevřít soubor
        if (!file.is_open()) {
            perror("Chyba, soubor passwd se nepodařilo otevřít");
            close(*new_fd);
            exit(EXIT_FAILURE);
        }
        bool recordWasFound = false; // příznak, že jsem doposud daný záznam nenalezli
        // čteme soubor passwd po řádcích
        while (getline(file, actualLine)) {
            match_results<string::const_iterator> PasswdItems; // obsahuje jednotlivé položky z passwd
            // zkontrolujeme, jestli je samotný formát řádku správný
            // chyba může nastat, pokud UID, GID nejsou čísla
            // nebo je v záznamu více položek
            // nebo některá položka obsahuje znak :
            // nebo login obsahuje neakceptovatelné znaky
            // nebo je některá z těchto položek prázdná (login, uid, gid, homedir)
            if (!regex_match(actualLine, PasswdItems, patternPasswd)) {
                fprintf(stderr, "Chyba: Špatný formát souboru! Následující řádek je chybný:\n");
                fprintf(stderr, "%s\n", actualLine.c_str());
                close(*new_fd);
                exit(EXIT_FAILURE);
            }

            // zkontrolujeme, jestli se jedná o požadovaný záznam a případně jej přidáme no výstupní zprávy
            if (searchBy == LOGIN) {
                string tmpStrLogin = string(PasswdItems[1]); // získáme login ze záznamu passwd
                // aktuální login načtený ze záznamu passwd porovnáme s hledaným loginem
                if (strcasecmp(tmpStrLogin.c_str(), vectorOfSearchedUsers[i].c_str()) == 0) {
                    *messageToSend += "<record type=\"normal\">";
                    // záznam jsme nalezli, teď postupně z něj získáme jen požadované položky
                    for (unsigned int j = 0; j < vectorOfRequiredItems.size(); j++) {
                        if (vectorOfRequiredItems[j] == "L") {
                            *messageToSend += string(PasswdItems[1]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "U") {
                            *messageToSend += string(PasswdItems[3]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "G") {
                            *messageToSend += string(PasswdItems[4]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "N") {
                            *messageToSend += string(PasswdItems[5]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "H") {
                            *messageToSend += string(PasswdItems[6]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "S") {
                            *messageToSend += string(PasswdItems[7]) + " ";
                        }

                    }
                    (*messageToSend).pop_back();   // poslední mezeru odstraníme
                    *messageToSend += "</record>";
                    recordWasFound = true;         // nastavíme příznak, že jsme nalezli daný záznam
                    break;                         // login je unikátní, takže už dále nebudeme hledat
                }
            } 
            else if (searchBy == UID) {
                string tmpStrUID = string(PasswdItems[3]);
                 // aktuální uid načtené ze záznamu passwd porovnáme s hledaným uid
                if (strcasecmp(tmpStrUID.c_str(), vectorOfSearchedUsers[i].c_str()) == 0) {
                    *messageToSend += "<record type=\"normal\">";
                    // záznam jsme nalezli, teď postupně z něj získáme jen požadované položky
                    for (unsigned int j = 0; j < vectorOfRequiredItems.size(); j++) {
                        if (vectorOfRequiredItems[j] == "L") {
                            *messageToSend += string(PasswdItems[1]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "U") {
                            *messageToSend += string(PasswdItems[3]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "G") {
                            *messageToSend += string(PasswdItems[4]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "N") {
                            *messageToSend += string(PasswdItems[5]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "H") {
                            *messageToSend += string(PasswdItems[6]) + " ";
                        } 
                        else if (vectorOfRequiredItems[j] == "S") {
                            *messageToSend += string(PasswdItems[7]) + " ";
                        }

                    }
                    (*messageToSend).pop_back();    // poslední mezeru odstraníme
                    *messageToSend += "</record>";
                    recordWasFound = true;          // nastavíme příznak, že jsme nalezli daný záznam
                    continue;                       // uid nemusí být unikátní, pokračujeme v prohledávání
                }
            }

        }
        // pokud při čtení ze souboru došlo k chybě
        if (file.bad()) {
            perror("Chyba, během čtení ze souboru passwd");
            close(*new_fd);
            exit(EXIT_FAILURE);
        }

        // pokud jsme nenalezli hledaný záznam, zapíšeme upozornění
        if (recordWasFound == false) {
            if (searchBy == LOGIN) {
                *messageToSend += "<record type=\"error\">Chyba: neznamy login " + vectorOfSearchedUsers[i] + "</record>";
            } 
            else {
                *messageToSend += "<record type=\"error\">Chyba: nezname UID " + vectorOfSearchedUsers[i] + "</record>";
            }
        }
        file.close(); // soubor uzavřeme
    }

    *messageToSend += "</records>";
    *messageToSend += "</message>";
}

void sendRequiredDataToClient(int* new_fd, string* messageToSend) {

    int bytes_sent; // počet bajtů, které v jednom volání poslal funkce write

    // postupně vrátíme požadovaná data klientovi
    while (1) {
        // pokusíme se odeslat všechna data na server
        if ((bytes_sent = write(*new_fd, (*messageToSend).c_str(), (*messageToSend).length())) < 0) {
            // chyba při posílání dat klientovi
            perror("Chyba: nepodařilo se odeslat data klientovi");
            close(*new_fd);
            exit(EXIT_FAILURE);
        }

        // pokud z nějakého důvodu nemohla být všechna data zaslána klientovi, 
        // připravíme poslání zbytku dat pro další iteraci
        if ((unsigned int) bytes_sent != (*messageToSend).length()) {
            *messageToSend = (*messageToSend).substr(bytes_sent, string::npos);
        }
        // pokud byla všechna data úspěšeně odeslána vyskočíme ze smyčky
        else {
            break;
        }
    }
}